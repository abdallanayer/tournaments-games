## Run the Project
- Navigate to the root folder
- add local.mogul.gg to ur hosts file
- run sudo docker-compose up
- Navigate to http://local.mogul.gg/ 

## Endpoints consumed
- api/Game/Search
- api/game/${gameShortCode}/Leaderboard/Skill`
- api/Tournament/Search/IdList
- api/Tournament/Summary
- API/Tournament/${tournamentId}?lastUpdatedDateTime
- API/Platform/Region/From/IP

## Routes implemented
- http://local.mogul.gg/ listing all torunaments
- http://local.mogul.gg/tournament/details/44541/forged-in-the-barrens-standard-opening-tournament tournament details
- http://local.mogul.gg/games/details/HS game details
