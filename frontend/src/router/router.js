import { createRouter, createWebHistory } from 'vue-router'
import TournmentList from '../views/tournament-list/tournament-list'
import SingleTournament from '../views/single-tournament/single-tournament'
import GameList from '../views/game-list/game-list'
import GameDetails from '../views/game-details/game-details'


const routes = [
    {
        path: '/',
        name: 'TournmentList',
        component: TournmentList,
    },
    {
        path: '/tournament/details/:id/:name',
        name: 'TournamentDetails',
        component: SingleTournament,
    },
    {
        path: '/games',
        name: 'GameList',
        component: GameList,
    },
    {
        path: '/games/details/:gameShortName',
        name: 'GameDetails',
        component: GameDetails,
    },

]

const Router = createRouter({
    history: createWebHistory(),
    routes
  })

export default Router
