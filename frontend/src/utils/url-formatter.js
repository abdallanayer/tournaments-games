export function formatStringUrl(str){
    return str.replace(/ /g, '-').toLowerCase();
}