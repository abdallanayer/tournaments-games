export function updateTextToAnchor(str){
    const urlRegex = /(https?:\/\/[^\s]+)/g;
    let formattedText =  str.replace(/(<([^>]+)>)/gi, "");
    formattedText = formattedText.replace(urlRegex,"<a target='_blank' href='$1'>$1</a>");
    return formattedText;
}