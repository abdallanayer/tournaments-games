import axios from 'axios'
import { CLIENT_API } from './base-urls.constant';

const HTTP_INSTANCE = axios.create({
    baseURL: `${CLIENT_API}`,
    timeout: 100000,
    headers: {
        'Content-Type': 'application/json',
        "arena-api-key": "C434EDE3-2E7E-4B9D-A070-58B2CF94846D"
    }
});

HTTP_INSTANCE.interceptors.response.use((response) => {
    if (!response.data.Success) {
        return Promise.reject();
    }
    return response.data.Response;
}, (error) => {
    return Promise.reject(error.message);
});

export default HTTP_INSTANCE;