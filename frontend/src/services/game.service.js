import HTTP_INSTANCE from "../http/http-instance";

export function getGameList() {
    return HTTP_INSTANCE.get(
        `api/Game/Search`
    ).then((response) => {
        return response;
    });
}

export function getGameDetails(gameShortCode) {
    const gameShortCodeFormatted = gameShortCode.toLowerCase();
    return getGameList().then((gameList) => {
        return gameList.find((game) =>
            game.GameShortCode.toLowerCase() === gameShortCodeFormatted
        )
    })
}

export function getGameTopLeaderShip(gameShortCode, top = 6) {
    return HTTP_INSTANCE.get(
        `api/game/${gameShortCode}/Leaderboard/Skill`
    ).then((response) => {

        return response.Leaderboard.slice(0, top);
    });
}