import HTTP_INSTANCE from "../http/http-instance";

let USER_REGION;
export function getUserRegion(){
    if(USER_REGION) return Promise.resolve(USER_REGION);
    return HTTP_INSTANCE.get(
        `API/Platform/Region/From/IP`
    ).then((response) => {
        USER_REGION = response;
        return response;
    });
}

export function initApp() {
    return HTTP_INSTANCE.get(
        `API/Platform/Application/Initialization?clientTime=${new Date().toISOString()}`
    ).then((response) => {
        return response;
    });
}