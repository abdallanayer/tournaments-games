import HTTP_INSTANCE from "../http/http-instance";
import { POLLING_API } from '../http/base-urls.constant';
import { getUserRegion } from '../services/app.service'

//TODO: util should be added to break filter array instead of hard coding value  tournamentRegionIdList[0]
export function getTournamentsIds(gameShortCode = "") {
    return getUserRegion().then((userRegion) => {
        return HTTP_INSTANCE.get(
            'api/Tournament/Search/IdList'
            , {
                params: {
                    "gameShortCodeList[0]": gameShortCode,
                    freeEntryOnly: false,
                    paidEntryOnly: false,
                    subscriberOnly: false,
                    favoritesOnly: false,
                    "tournamentRegionIdList[0]": userRegion.GamingServerRegionId,
                    searchTerms: "",
                    ListType: "upcoming",
                    "Paging.SortByName": "StartDate",
                    "Paging.SortDirection": "asc"
                }
            }).then((response) => {
                return response;
            });
    })

}

export function getTournamentList(gameShortCode = "") {
    return getTournamentsIds(gameShortCode).then((tournmentIds) => {
        const tournmentIdsFormatted = tournmentIds.map((tournment) => (
            { TournamentId: tournment.TournamentId }
        ));
        return HTTP_INSTANCE.post("api/Tournament/Summary", tournmentIdsFormatted).then((response) => {
            return response;
        });
    });
}

export function getTournamentDetails(tournamentId) {
    return HTTP_INSTANCE({ url: `${POLLING_API}API/Tournament/${tournamentId}?lastUpdatedDateTime` }).then((response) => {
        return response;
    });
}